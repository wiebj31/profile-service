import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {getRepository, getManager, Repository} from "typeorm";
import { Profile } from './Entity/Profile';

@Injectable()
export class AppService {

  constructor(
    @InjectRepository(Profile)
    private profileRepository: Repository<Profile>,
  ) {}
  
  async getById(profileId: string): Promise<Profile> {
    //TO DO magische database dingen.
    return this.profileRepository.findOne({'sub': profileId});
  }

  async getAll(){
    //TO DO magische database dingen.
    return this.profileRepository.find();
  }

  async update(profile: Profile): Promise<String> {
    this.profileRepository.update(profile.id, profile);
    return 'updated';
  }

  async create(profile: Profile): Promise<String>{
    const res_profile = await this.getById(profile.sub);
    if(res_profile == undefined){
      this.profileRepository.save(profile)
      return 'created';
    }
    return 'already exists';
  }

  async delete(profileId: number): Promise<String>{
    this.profileRepository.delete(profileId)
    return 'deleted';
  }
}
