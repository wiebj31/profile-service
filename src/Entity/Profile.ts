import { Entity, Column, PrimaryGeneratedColumn, PrimaryColumn  } from 'typeorm'

@Entity('profile')
export class Profile{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    sub: string;

    @Column()
    name: string;

    @Column()
    bio: string;
}