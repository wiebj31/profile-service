import { Body, Controller, Delete, Get, Param, Post, Put, Patch } from '@nestjs/common';
import { AppService } from './app.service';
import { MessagePattern } from '@nestjs/microservices';
import {getRepository, getManager} from "typeorm";
import { Profile } from './Entity/Profile';
import BadRequestException from './exceptions/badRequest.exception';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  // define the message pattern for this method
  @MessagePattern('/profiles')
  @Get('/profile/:id')
  async getProfileById(@Param("id") profileId: string) {
    return await this.appService.getById(profileId);
  }

  @Get('/profiles')
  async getAll() {
    return await this.appService.getAll();
  }

  @Patch('/update')
  async update(@Body() profile: Profile) {
    const res_profile: Profile = await this.appService.getById(profile.sub);
    res_profile.sub = profile.sub;
    if (!res_profile) {
      throw new BadRequestException(
        `Project with id ${profile.sub} does not exist... (ಥ﹏ಥ)`,
      );
    }

    Object.keys(profile).forEach((key) => {
      if (!res_profile.hasOwnProperty(key)) {
        throw new BadRequestException(
          `Property ${key} does not exist on type Project...`,
        );
      } else {
        res_profile[key] = profile[key];
      }
    });

    return await this.appService.update(res_profile);
  }

  @Post('/profile')
  async create(@Body() profile: Profile){
    return await this.appService.create(profile);
  }

  @Delete('/delete/:id')
  async delete(@Param("id") profileId: number){
    return await this.appService.delete(profileId);
  }
}
