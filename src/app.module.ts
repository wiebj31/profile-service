import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Profile } from './Entity/Profile';


@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'MATH_SERVICE',
        transport: Transport.REDIS,
        options: {
          url: 'redis://localhost:6379',
          auth_pass: 'redispassword'
        }
      },
    ]),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '178.84.152.77',
      port: 3306,
      username: 'root',
      password: 'wachtwoord',
      database: 'Profile',
      entities: [Profile],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Profile])
  ],
  controllers: [AppController],
  providers: [AppService],

})
export class AppModule {}
