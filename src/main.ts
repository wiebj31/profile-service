import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

const logger = new Logger('Main');

const microservicesOptions = {
  transport: Transport.REDIS,
  options: {
    url: 'redis://localhost:6379',
    auth_pass:'redispassword'
  },
} as MicroserviceOptions; 

async function bootstrap() {
  const app = await NestFactory.create(
    AppModule,
  );
  app.enableCors()
  app.connectMicroservice<MicroserviceOptions>(microservicesOptions);
  app.listen(3000,() => {
    logger.log('microservice is listening ...');
  });
}

bootstrap();
